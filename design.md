---
layout: page
title: Design
menu: ignore
---

<style>
.post-content img {
    display: block;
    float: left;
    padding: 1em;
}
.post-content h1 {
    clear:both
}
.post-content p {
    overflow: hidden;
}
/* .post-content p + p {
    background-color: #2e2e2e;
} */

.post-content img:nth-of-type(1),
.post-content img:nth-of-type(2),
.post-content img:nth-of-type(3) {
    background-color: #fdfdfd;
}
.post-content img:nth-of-type(4),
.post-content img:nth-of-type(5),
.post-content img:nth-of-type(6) {
    background-color: #2e2e2e;
}

</style>


# logo brand

![]({{ "/assets/logo-brand.svg" | relative_url }}){:width="320px" height="111.15px"}
![]({{ "/assets/logo-brand-monochrome.svg" | relative_url }}){:width="320px" height="111.15px"}
![]({{ "/assets/logo-brand-black.svg" | relative_url }}){:width="320px" height="111.15px"}
![]({{ "/assets/logo-brand-inverted.svg" | relative_url }}){:width="320px" height="111.15px"}
![]({{ "/assets/logo-brand-monochrome-inverted.svg" | relative_url }}){:width="320px" height="111.15px"}
![]({{ "/assets/logo-brand-white.svg" | relative_url }}){:width="320px" height="111.15px"}

# logo short

![]({{ "/assets/logo-short.svg" | relative_url }}){:width="125px" height="60px"}
![]({{ "/assets/logo-short-monochrome.svg" | relative_url }}){:width="125px" height="60px"}
![]({{ "/assets/logo-short-black.svg" | relative_url }}){:width="125px" height="60px"}
![]({{ "/assets/logo-short-inverted.svg" | relative_url }}){:width="125px" height="60px"}
![]({{ "/assets/logo-short-monochrome-inverted.svg" | relative_url }}){:width="125px" height="60px"}
![]({{ "/assets/logo-short-white.svg" | relative_url }}){:width="125px" height="60px"}

# logo brand long

![]({{ "/assets/logo-brand-long.svg" | relative_url }}){:width="800px" height="111px"}
![]({{ "/assets/logo-brand-long-monochrome.svg" | relative_url }}){:width="800px" height="111px"}
![]({{ "/assets/logo-brand-long-black.svg" | relative_url }}){:width="800px" height="111px"}
![]({{ "/assets/logo-brand-long-inverted.svg" | relative_url }}){:width="800px" height="111px"}
![]({{ "/assets/logo-brand-long-monochrome-inverted.svg" | relative_url }}){:width="800px" height="111px"}
![]({{ "/assets/logo-brand-long-white.svg" | relative_url }}){:width="800px" height="111px"}

# Farben

<span class="brand-color-box brand-yellow">#ffd42a</span>
<span class="brand-color-box brand-green">#00a600</span>
<span class="brand-color-box brand-black">#2e2e2e</span>
<span class="brand-color-box brand-black-inverted">#fdfdfd</span>
<span class="brand-color-box brand-yellow-monochrome">#d6d6d6</span>
<span class="brand-color-box brand-green-monochrome">#828282</span>


# Schrift

[Titillium](https://www.fontsquirrel.com/fonts/Titillium) (SIL Open Font License v1.10)

* Regular
* *Regular Italic*
* **Semibold**
* ***Semibold Italic***
