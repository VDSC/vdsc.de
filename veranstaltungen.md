---
layout: page
title: Veranstaltungen der Dresdner Studentenclubs
menu: Veranstaltungen
order: 1.5
append_head: |
  <link rel="preload" href="https://events.vdsc.de/calendar.json" as="fetch" />
  <script src="assets/events.js" defer=""></script>
  <script>
    document.addEventListener("DOMContentLoaded", () => {
      create_event_table(document.querySelector("#events > .caption"));
    });
  </script>
---

<span class="caption">Veranstaltungen werden geladen...</span>
{: #events}

Die Veranstaltungen werden stündlich von den Websiten der [Clubs](clubs.html) aktualisiert und können [hier](https://events.vdsc.de/calendar.ics) abonniert werden.
