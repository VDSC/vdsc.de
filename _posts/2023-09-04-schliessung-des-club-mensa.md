---
layout: post
title:  "Schließung des Club Mensa"
date:   2023-09-04 18:00
author: Burts
---
Der [Club Mensa (CM)][1] im Hochschulgebäude Reichenbachstraße schließt. Mit Bedauern müssen wir die Insolvenz des Club Mensa e.V. hinnehmen. Nach 57 Jahren Clubbetrieb schloss der CM zum Semesterende im Juni seine Pforten. Inzwischen wurde das Insolvenzverfahren eröffnet. Wirtschaftliche Probleme in der Nach-Corona-Zeit haben dem Club schwer zu schaffen gemacht und einen tragfähigen Weiterbetrieb verhindert. Wenn auch die Zusammenarbeit nicht immer reibungslos funktioniert hat, so war der Club Mensa doch stets verlässlicher Mitstreiter der Dresdner Clubkultur und Mitorganisator großer Dresdner Clubevents wie der Nachtwanderung oder dem Uni-Air.

[1]: https://clubmensa.de/
