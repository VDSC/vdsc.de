---
layout: post
title:  "Die beste alternative Spielstätte ist studentisch!"
date:   2023-10-25
author: Sophie
---

Das [„Kino im Kasten“][1] wurde im September für sein [hervorragendes Jahresprogramm 2022 ausgezeichnet][2].  
„Dresdens Stadtteil Strehlen ist durchaus zu beneiden“ so der Filmkritiker Norbert Wehrstedt im Rahmen der Verleihung.  
Die Verleihung des Kinoprogrammpreises wurde durch die Mitteldeutschen Medienförderung GmbH ermöglicht und zeigt, dass es für ein gutes Programm nicht unbedingt ausgefallener Technik bedarf.

Das Kino im Kasten wird, wie auch die anderen Dresdner Studentenclubs, von ehrenamtlichen Mitgliedern betrieben, welche mit viel Engagement und Herzblut für ein vielfältiges Programm sorgen und damit einen erheblichen Mehrwert für die Kultur in Dresden bieten.

[1]: https://www.kino-im-kasten.de/
[2]: https://www.sendcockpit.com/appl/ce/software/code/ext/_ns.php?&uid=551266cea5b108cdf65619dab6a41a05