---
layout: post
title:  "NaWa-Festival statt Nachtwanderung"
date:   2022-05-19 14:30
author: Sophie
---
Immer noch völlig überwältigt blicken wir auf das erste NaWa-Festival am 17. Mai 2022 und sagen: ein riesiges Danke an EUCH! Niemals hätten wir damit gerechnet, dass so viele von euch mit den Clubs bis tief in die Nacht spielen, tanzen, feiern und natürlich auch trinken. Die letzten 2 Jahre sind nicht spurlos an den [Dresdner Studentenclubs][1] vorbei gegangen. Umso schöner ist es, dass ihr den Clubs immer noch treu seid und möglich macht, dass diese immer weiter schöne Abende für euch veranstalten können.

Ein riesiges Danke geht natürlich auch an das [Studentenwerk Dresden][2], die tatkräftig an unserer Seite waren und für das tolle musikalische Programm gesorgt haben. Danke an dieser Stelle auch an die DJs MASN und Bounthill, an die HfM Dresden und an Road To You. Danke außerdem an die [TU Dresden][3], die uns das überhaupt erst ermöglicht hat und den [StuRa][4].

Und vor allem geht ein fettes Danke an die ehrenamtlichen Mitglieder der 11 Studentenclubs, die geplant, vorbereitet, bespaßt, gemixt, geschleppt und mitgetrunken haben.

[1]: https://vdsc.de/clubs/
[2]: https://www.studentenwerk-dresden.de/kultur/
[3]: https://www.tu-dresden.de
[4]: https://www.stura.tu-dresden.de
