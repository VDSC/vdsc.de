---
layout: post
title:  "Vorfreude auf die Dresdner Studententage im Mai"
date:   2022-04-20 14:30
author: Burts
---
Seit Wochen feilen die [Studentenclubs][1] gemeinsam mit dem [Studentenwerk Dresden][2] an der Planung der diesjährigen [Studententage][3] – dem größten studentischen Kulturevent Mitteldeutschlands.
Vom 3. bis zum 24. Mai bieten die Dresdner Studentenclubs und einige künstlerische Gruppen der Dresdner Hochschulen ein vielfältiges kulturelles Programm.

Ob beim Wiesenkonzert der Clubs Novitatis und Wu5 oder dem MittelAlterFest von Traumtänzer und GAG 18 – endlich besteht wieder die Möglichkeit, Live-Atmosphäre zu schnuppern, Freunden zu begegnen und neue Menschen „live und in Farbe“ kennenzulernen.

Der Universitätschor Dresden lädt zu zwei Konzerten in die Lukaskirche ein. Zusammen mit SolistInnen des Mozarteums Salzburg und SolistInnen der Schlagzeugklasse der Hochschule für Musik Dresden wagen sie sich an zwei Klassiker der Chorliteratur: Carl Orffs „Carmina Burana“ und Jonathan Doves „The Passing of the Year“.

Und natürlich laden neben diesen größeren Groß-Veranstaltungen auch die anderen Clubs zu einem Besuch ein. Wer schon immer mal wissen wollte, was den Unterschied zwischen einem Bourbon oder Scotch ausmacht oder was man sich unter molekularen Cocktails vorstellen kann, sollte sich die Termine im Gutzkowclub oder Aquarium vormerken.

Weitere Termine mit Theater, Kino und Musik sind derzeit noch in Planung – bleibt also neugierig.

[1]: https://vdsc.de/clubs/
[2]: https://www.studentenwerk-dresden.de/kultur/
[3]: https://dresdner-studententage.de/
