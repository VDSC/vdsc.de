---
layout: post
title:  "VDSC beim Infotreff"
date:   2014-05-06 12:00
author: Marc
---

Kommt alle zum VDSC-Stand beim [Infotreff der Dresdner Studententage][16] an der Mensa Bergstraße, gewinnt gegen uns und nehmt Gimmicks mit!

[16]: https://www.exmatrikulationsamt.de/6070176/
