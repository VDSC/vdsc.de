---
layout: post
title:  "Campusfest zur Feierlichen Immatrikulation"
date:   2022-10-06 10:00
author: Burts
---

Heute lädt die TU Dresden alle neuen Studierenden zur [Feierlichen Immatrikulation][1] ins Hörsaalzentrum. Zum Campusfest an der HSZ-Wiese sind auch die Dresdner Studentenclubs wieder vor Ort, und versorgen Euch ab 16:00 Uhr mit Cocktails und alkoholfreien Getränken.

[1]: https://tu-dresden.de/studium/im-studium/studienstart/immatrikulationsfeier
