---
layout: post
title:  "Dresdner Studententage 2022"
date:   2022-05-03 12:00
author: Burts
---
Heute beginnen die [Dresdner Studententage 2022][1]. Gemeinsam mit dem [Studentenwerk Dresden][2] haben die [Dresdner Studentenclubs][3] ein vielfältiges und abwechslungsreiches Programm auf die Beine gestellt, bei dem garantiert für jeden Geschmack etwas dabei ist: Barabend, Karaoke, Filmabend, Kneipenquiz, Skat-, Kicker- oder Dartturniere, Theater und natürlich Konzerte. Besonders genannt sei die neue Ausstellungsreihe [Wasserwerke][4] im Club Aquarium, das [WiesenWudstok][5] Open Air der Clubs Novitatis und Wu5 und das [MittelAlterFest][6] der Clubs GAG18 und Traumtänzer. Leider kann es auch in diesem Jahr keine Nachtwanderung geben - dafür laden die Clubs zum [NaWa-Festival][7] auf den Campus. Das ganze Programm der Studententage gibt es unter https://dresdner-studententage.de.

[1]: https://dresdner-studententage.de
[2]: https://www.studentenwerk-dresden.de/kultur/
[3]: https://vdsc.de/clubs/
[4]: https://club-aquarium.de/events/ausstellungseroeffnung/
[5]: https://dresdner-studententage.de/events/103
[7]: https://dresdner-studententage.de/events/99
[6]: https://www.mittelalterfeste.eu/
