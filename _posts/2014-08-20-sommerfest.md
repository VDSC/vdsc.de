---
layout: post
title:  "Sommerfest"
date:   2014-08-20 12:00
author: Marc
---

Wir laden die Mitglieder aller Studentenclubs in Deutschland zum [50JDSC Sommerfest][15] ein! Es gibt viele sportlich-spaßige Wettbewerbe und gute Gelegenheiten zum Kontakte pflegen und Erfahrungen austauschen.

[15]: https://www.exmatrikulationsamt.de/6071356
