---
layout: post
title:  "Studentenclubs wieder geschlossen"
date:   2021-11-21 17:30
author: Burts
---
Aufgrund der Infektionslage und der Überlastungsstufe der sächsischen Krankenhäuser, müssen die Studentenclubs nun ab Montag wieder alle geschlossen bleiben. Dies gilt vorerst bis 12. Dezember. Es wird sich zeigen, wie sich die Lage entwickelt und wie es dann weitergeht.
