---
layout: post
title:  "Vorentscheide zum UNI AIR"
date:   2023-04-11 14:00
author: Burts
---

Bevor es [am 24. Mai auf die große Bühne][1] geht, stehen für die studentischen Nachwuchsbands noch Qualifikations-Vorentscheide aus. Dafür laden die Clubs zusammen mit dem [Studentenwerk][2] am 12. April in den [Club Bärenzwinger][3], am 21. April in den [Club Aquarium][4] und am 27. April in den [Club Novitatis][5]. Neben der Jury-Entscheidung ist die Stimme der Zuschauer entscheidend – kommt also zahlreich und votet für eure Lieblingsband!

[1]: https://uni-air.de
[2]: https://studentenwerk-dresden.de/kultur/
[3]: https://www.baerenzwinger.de/Veranstaltungen/
[4]: https://club-aquarium.de/2023/04/vorentscheid-zum-uni-air-2023/
[5]: https://novitatis.de
