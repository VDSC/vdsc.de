---
layout: post
title:  "Dresdner Studententage 2014"
date:   2014-05-05 12:00
author: Marc
---

Da sind sie wieder, die Dresdner Studententage. Wir haben wieder mal ein [vielfältiges Programm][17] für euch vorbereitet und freuen uns auf euren Besuch!

[17]: https://dresdner-studententage.de/2014/veranstaltungen
