---
layout: post
title:  "Studentenclubs sammeln Kleidung für Flüchtlinge"
date:   2015-11-15 12:00
author: Burts
---

Ihr könnt helfen! In der Erstaufnahmeeinrichtung für Flüchtlinge auf der Bergstraße 51 wird dringend Bekleidung benötigt. Vom 16. bis 20. November findet eine Bekleidungssammlung für Flüchtlinge statt. Infos zu benötigter Kleidung, Sammelstellen und -zeiten findet ihr [bei eXma][6].

[6]: https://www.exmatrikulationsamt.de/6075449
