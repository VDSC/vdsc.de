---
layout: post
title:  "Neues Clubheft erschienen"
date:   2020-10-20 20:00
author: Burts
---

Zum Start des Wintersemesters 2020/21 ist nun pünktlich zur verkleinerten ESE-Clubtour eine [aktualisierte Ausgabe des Studentenclubheftes][1] erschienen. Auf 40 Seiten stellen sich die 15 Dresdner Studentenclubs kurz vor. Außerdem gibt es Gutscheine für jeden Club. Die Clubhefte gibt es in den Clubs, auf dem Campus und [hier als PDF][1].

[1]: /clubheft.pdf