---
layout: post
title:  "Dresdner Studententage 2017"
date:   2017-05-01 12:00
author: Burts
---

Auch dieses Jahr bieten die Studententage wieder [allerhand Programm][2] über drei Wochen. Absoluter Höhepunkt natürlich wieder die [Nachtwanderung][3] am 16. Mai!

[2]: https://www.dresdner-studententage.de/2017/veranstaltungen
[3]: https://www.dresdner-nachtwanderung.de/
