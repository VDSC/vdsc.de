---
layout: post
title:  "50 Jahre Dresdner Studentenclubs"
date:   2014-04-01 12:00
author: Marc
---


2014 läuft alles nach dem Motto „50 Jahre Studentenclubs“, denn der Gutzkowclub wird als ältester Dresdner Studentenclub dieses Jahr 50.

Alles weitere dazu auf [vdsc.de/50](/50)

