---
layout: post
title:  "Die Dresdner Studententage stehen auf der Kippe"
date:   2021-04-27 13:30
author: Burts
---
… und können nach jetziger Einschätzung der Situation wohl nicht wie geplant stattfinden.
Die Neufassung des Infektionsschutzgesetzes und die weiterhin hohen Zahlen an Corona-Neuinfektionen lassen die Hoffnung schwinden, dass die Durchführung von größeren Veranstaltungen mit Publikum im Juni möglich sein wird. Gemeinsam mit dem [Studentenwerk Dresden][1] überlegen die [Dresdner Studentenclubs][2], wie zumindest ein Teil der geplanten Veranstaltungen durch alternative Formate ersetzt werden kann.

[1]: https://www.studentenwerk-dresden.de/kultur/
[2]: https://vdsc.de/clubs.html