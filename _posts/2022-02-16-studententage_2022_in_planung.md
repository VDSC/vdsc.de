---
layout: post
title:  "Studententage 2022 in Planung"
date:   2022-02-16 17:30
author: Burts
---
Während die Studentenclubs noch geschlossen sind, bleiben ihre Mitglieder nicht untätig. Wir sind bereits fleißig an der Planung für das Sommersemester. Im Mai sollen auch in diesem Jahr die Dresdner Studententage stattfinden. Während die Durchführbarkeit der Nachtwanderung noch fraglich ist, sind andere Veranstaltungen, wie das MittelAlterFest, Open-Air-Konzerte oder kleinere thematische Clubabende, schon fest geplant.
