---
layout: post
title:  "UNI AIR 2024 – Band- und DJ-Contest"
date:   2024-06-10 10:00
author: Burts
---
Im Rahmen der [Dresdner Studententage][1] findet am 12.06.2024 ab 18:00 Uhr erneut das [UNI AIR][2] auf der Wiese hinter dem Hörsaalzentrum statt. Das UNI AIR ist eine Kooperationsveranstaltung des [Studentenwerks][3] mit den Dresdner [Studentenclubs][4], die gemeinsam für die Bands, DJs und Gäste einen unvergesslichen Abend veranstalten. 

Bei dem studentischen Musikwettbewerb treten studentische Bands (dieses Jahr: von Jacobi, Infrapop, Home Office, Six Good Years, Jamarula und Blan Fleur) und DJs (dieses Jahr: DJ Franzl, Sonza, Omar, Yudo und harzbeats) gegeneinander an.

Als Zwischenakt dürfen sich die Gäste auf [AMPTRAY][5] freuen, welche bereits im vorangegangenen Jahr erfolgreich das UNI AIR gerockt haben. 

[1]: https://dresdner-studententage.de/
[2]: https://www.studentenwerk-dresden.de/kultur/uni-air-bandcontest.html
[3]: https://www.studentenwerk-dresden.de/
[4]: https://vdsc.de/clubs.html
[5]: https://www.amptray.de/