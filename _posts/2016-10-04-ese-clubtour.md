---
layout: post
title:  "ESE-Clubtour"
date:   2016-10-04 12:00
author: Burts
---

Kurz vor dem richtigen Start ins Wintersemester gibt es wieder die ESE-Clubtour! Zusammen mit den Fachschaften der TU und HTW laden die Dresdner Studentenclubs wieder alle Erstis zur Schuppertour durch die Clubs. Für die ganz Neugierigen gibt es auch Freigetränke. Alles Wichtige dazu steht [bei eXma][4].

[4]: /eseclubtour
