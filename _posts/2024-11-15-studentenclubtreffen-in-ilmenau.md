---
layout: post
title:  Studentenclubtreffen in Ilmenau
date:   2024-11-15 00:00
author: burts
---

An diesem Wochenende veranstalten die [Ilmenauer Studentenclubs][1] das [30. Ilmenauer Wettrödeln][2].
Der feucht-fröhliche Freizeitsport-Event ist gleichzeitig das größte Treffen
der deutschen Studentenclubs. Neben den 4½ Ilmenauer Clubs nehmen 26 weiter
Studentenclubs teil. Allein aus Dresden haben neun Clubs die Reise nach
Thüringen angetreten. Wir wünschen viel Spaß und viel Erfolg!

[1]: https://www.il-sc.de/
[2]: https://www.wettroedeln.de/
