---
layout: post
title:  "K(l)eine Dresdner Studententage"
date:   2021-05-31 13:25
author: Burts
---
Noch immer hoffen wir auf eine „Miniatur-Variante“ der diesjährigen [Dresdner Studententage][1]…
Schon jetzt ist klar, dass auf Grund der geltenden Corona-Verordnungen keine Großveranstaltungen stattfinden können. Und so sind ganz im Stil der letzten Monate einige digitale Veranstaltungen vorbereitet.
Angesichts sinkender Werte besteht nun eventuell doch die Möglichkeit, im Juni mit kurzfristigen Terminen im Freien – zum Beispiel kontaktarmen Sport-Veranstaltungen, Kultur-Picknick auf der Wiese mit Live-Musik oder Pop-up-Konzerten – zumindest eine kleine Variante der Studententage durchführen zu können.
Gemeinsam mit dem [Studentenwerk Dresden][2] sind die [Dresdner Studentenclubs][3] derzeit dabei, sich ein kreatives Konzept für diese Art „abgespeckter“ Studententage zu überlegen.

[1]: https://dresdner-studententage.de/
[2]: https://www.studentenwerk-dresden.de/kultur/
[3]: https://vdsc.de/clubs.html
