---
layout: post
title:  "Drei Tage Mittelalter"
date:   2023-05-12 14:00
author: Burts
---

Heute startet das dreitägige [MittelAlterFest][1] von und mit dem [Kellerklub GAG18][2] und dem [Club Traumtänzer][3]. Auf der Wiese hinter dem Wohnheim Fritz-Löffler-Straße 16/18 gibt's bis Sonntag mitten in der Stadt mittelalterliches Ambiente, Live-Musik, buntes Markttreiben, Speis und Trank, Kinderprogramm u.v.m. …

[1]: https://www.mittelalterfeste.eu/
[2]: https://www.gag-18.com/
[3]: https://club-traumtaenzer.de/