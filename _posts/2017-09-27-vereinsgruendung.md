---
layout: post
title:  "Vereinsgründung"
date:   2017-09-27 12:00
author: Burts
---

Nach zehnjährigem Bestehen hat sich die Vereinigung Dresdner Studentenclubs nun als Verein gegründet. Der **vdsc e.V.** soll als Dachverband die Arbeit der Studentenclubs fördern, ihre Zusammenarbeit stärken und die Dresdner Studentenclubs als Ganzes nach außen vertreten.

Zum Start waren zehn der Studentenclubs dabei – die restlichen werden demnächst eintreten. Die Gemeinnützigkeit wird beantragt.
