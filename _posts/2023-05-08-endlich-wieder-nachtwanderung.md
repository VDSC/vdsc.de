---
layout: post
title:  "Endlich wieder Nachtwanderung!"
date:   2023-05-08 17:30
author: Burts
---

Nachdem wir 3 Jahre eine Pause einlegen mussten, gibt es endlich wieder die [Dresdner Nachtwanderung][1]!

Am Dienstag, den 9. Mai öffnen 14 Studentenclubs gemeinsam an einem Abend mit Bands und DJs. Einmal Eintritt zahlen und schon stehen euch ab 19:00 Uhr alle die Türen zu alle Clubs offen. Und mit dem Shuttle-Bus geht es von Club zu Club! Dazu gibt es in jedem dritten Club Freigetränke.

[1]: https://www.dresdner-nachtwanderung.de/
