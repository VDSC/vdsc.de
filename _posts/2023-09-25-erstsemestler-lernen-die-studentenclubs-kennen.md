---
layout: post
title:  Erstsemestler lernen die Studentenclubs kennen
date:   2023-09-25 00:00
author: Elli
---

In wenigen Tagen startet die Vorlesungszeit der Dresdner Hochschulen für das Wintersemester 2023/2024. Im Rahmen der Vorbereitungswochen können die Neustudierenden durch die sogenannte „ESE“-Tour Clubluft schnuppern.

Am [28. September][1] führt der [StuRa der HTW Dresden][2] seine Erstis durch die [Studentenclubs][3] im Dresdner Süden und am [3. Oktober][4] laden dann die [Fachschaftsräte der TU Dresden][5] ihre Erstis zur ESE-Clubtour ein.

Wir wünschen allen eine lustige und interessante Tour und natürlich einen guten Semesterstart!

[1]: https://vdsc.de/htwese
[2]: https://www.stura.htw-dresden.de/
[3]: https://vdsc.de/clubs
[4]: https://vdsc.de/eseclubtour
[5]: https://www.stura.tu-dresden.de/fachschaften
