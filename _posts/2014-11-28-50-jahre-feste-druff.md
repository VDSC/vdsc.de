---
layout: post
title:  "50 Jahre Feste Druff"
date:   2014-11-28 12:00
author: Marc
---

Den goldenen Abschluss des Festmonats feiern wir morgen abend im Tusculum. Kommt alle vorbei und feiert mit uns noch einmal 50 Jahre Dresdner Studentenclubs. Vergesst euer hoffentlich vollgestempeltes Clubheft nicht!
