---
layout: post
title:  "10 Jahre Studentenclub Traumtänzer"
date:   2019-06-13 14:00
author: Burts
---

Kaum zu glauben, dass es schon 10 Jahre her ist, dass der [Studentenclub Traumtänzer][1] gegründet wurde. 2009 in den brachliegenden Räumen des vormaligen „Club P5“ gegründet, mit dem Vorsatz irgendwie anders zu sein, hat sich der Traumtänzer seinen festen Platz in der Clublandschaft erarbeitet. Vor nunmehr 1½ Jahren folgte dann der Umzug in die leerstehenden Räume des „Club New Feeling“ an der Budapester Straße, wo man sich inzwischen gut eingelebt hat.
Wir wünschen auch weiterhin viele nette Gäste, aktive Mitglieder und gute Ideen!

[1]: https://club-traumtaenzer.de/