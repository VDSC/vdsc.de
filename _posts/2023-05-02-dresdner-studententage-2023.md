---
layout: post
title:  "Dresdner Studententage 2023"
date:   2023-05-02 12:00
author: Burts
---

Heute beginnen die [Dresdner Studententage][1] 2023. Vom 2. bis 27. Mai bieten die [Studentenclubs][2] und künsterlischen Gruppen der Dresdner Hochschulen in Kooperation mit dem [Studentenwerk][3] ein vielfältiges kulturelles Programm. Im [Veranstaltungskalender][1] findet sich eine Vielzahl von Konzerten, Ausstellungen, Live-Musik und Partys. Insbesondere zu nennen sind das [&#x5B;ˈwʊdˌstɒk&#x5D; Wiesen-Open-Air][4] am 6. Mai, das [MittelAlterFest][5] vom 12.&ndash;14. Mai sowie das Finale des [UniAir][6]-Bandcontest am 24. Mai. Den Höhepunkt bildet in diesem Jahr endlich wieder die [Dresdner Nachtwanderung][7] durch alle Studentenclubs am 9. Mai.

[1]: https://dresdner-studententage.de/
[2]: https://vdsc.de/clubs.html
[3]: https://studentenwerk-dresden.de/kultur/
[4]: https://dresdner-studententage.de/events/125
[5]: https://www.mittelalterfeste.eu/
[6]: https://uni-air.de/
[7]: https://www.dresdner-nachtwanderung.de/
