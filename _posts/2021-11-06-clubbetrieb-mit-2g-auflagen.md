---
layout: post
title:  "Clubbetrieb mit 2G-Auflagen"
date:   2021-11-06 13:00
author: Burts
---
Entsprechend der neuen Sächsischen Corona-Schutz-Verordnung gelten ab Montag in allen Studentenclubs die verschärften 2G-Auflagen. Die Clubs können aber weiter geöffnet bleiben. Es gilt Zutritt nur mit Impfnachweis bzw. Genesenennachweis sowie Maskenpflicht in den öffentlichen Bereichen. Die Clubs haben weiter zum regulären Clubbetrieb geöffnet. Die Durchführbarkeit geplanter größerer Veranstaltungen wird im Einzelfall geprüft.
