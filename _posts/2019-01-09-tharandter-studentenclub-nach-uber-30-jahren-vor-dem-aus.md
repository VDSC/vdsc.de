---
layout: post
title:  "Tharandter Studentenclub nach über 30 Jahren vor dem Aus?"
date:   2019-01-09 20:00
author: Andreas Nicht
---

## Offener Brief im Namen der Dresdner Studentenclubs:

Das Studentenwohnheim Weißiger Höhe 1 in Tharandt wurde Ende 2018 vom Freistaat Sachsen an einen Investor verkauft. Wie jetzt bekannt wurde, müssen alle Bewohner, entgegen ursprünglicher Absprachen, kommenden April ausziehen.

Betroffen davon ist auch der Heinrich-Cotta-Club, der einzige Studentenclub Tharandts, welcher 1986 als Zusammenschluss Studierender der ansässigen Fachrichtung Forstwissenschaften gegründet wurde. Seit über drei Jahrzehnten sorgt dieser für eine lebendige, attraktive und weltoffene studentische Kultur in Tharandt. Schließlich ist er nicht nur eine Kneipe für die Tharandter Studenten, sondern auch einer der wenigen aktiven Kulturträger der Stadt. Die ehrenamtlich arbeitenden Mitglieder organisieren regelmäßig Diskussionsabende mit Wissenschaftlern oder jährliche Highlights wie das Holzhackerfest oder den Grünen Ball. Somit hat der Heinrich-Cotta-Club über die Universität hinaus für ganz Tharandt eine große Bedeutung. Diese lange Tradition steht aufgrund fehlender alternativer Räumlichkeiten jetzt vor einer ungewissen Zukunft.

Die Vereinigung Dresdner Studentenclubs (VDSC) spricht sich mit Nachdruck für den Erhalt des Heinrich-Cotta-Clubs aus und fordert alle beteiligten Institutionen auf, den Club beim Finden einer geeigneten Lösung zu unterstützen. Dieser Appell richtet sich in erster Linie an das Studentenwerk Dresden, die Technische Universität Dresden, die Stadt Tharandt und den Landkreis Sächsische Schweiz-Osterzgebirge. Der Heinrich-Cotta-Club muss als wichtiger studentischer Kulturträger in Tharandt erhalten bleiben, um die Attraktivität des Studiums in der Forststadt Tharandt langfristig zu sichern!
