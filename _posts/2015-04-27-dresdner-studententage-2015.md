---
layout: post
title:  "Dresdner Studententage 2015"
date:   2015-04-27 12:00
author: Marc
---

Es ist wieder soweit. Knapp ein Monat vollgepackt mit [kleinen und großen Veranstaltungen][9] in den Dresdner Studentenclubs steht vor euch. Kommt vorbei, wir freuen uns auf euch!

[9]: https://dresdner-studententage.de/2015/veranstaltungen
