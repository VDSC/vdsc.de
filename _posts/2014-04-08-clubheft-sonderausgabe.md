---
layout: post
title:  "Clubheft Sonderausgabe"
date:   2014-04-08 12:00
author: Marc
---

Zum 50. Jubiläum gibt es eine Sonderausgabe des Clubhefts. Ab sofort auf dem Campus und in den Clubs zu finden, oder hier als [PDF][1].

[1]: /archiv/clubheft-v50.pdf
