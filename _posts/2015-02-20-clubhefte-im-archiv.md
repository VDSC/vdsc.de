---
layout: post
title:  "Clubhefte im Archiv"
date:   2015-02-20 12:00
author: Marc
---

Die [Slub][10] hat jetzt je ein Exemplar unserer Clubheft-Editionen im Archiv gelagert. Gleichzeitig haben wir sie [in unserem Archiv][11] jetzt auch verlinkt.

[10]: https://www.slub-dresden.de
[11]: /archiv/archiv1.html
