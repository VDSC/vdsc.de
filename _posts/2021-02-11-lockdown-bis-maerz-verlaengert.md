---
layout: post
title:  "Lockdown bis März verlängert"
date:   2021-02-11 12:15
author: Burts
---

Der Lockdown wird nun bis 7. März verlängert, und damit bleiben auch die Studentenclubs weiter geschlossen. Es lässt sich jedoch derzeit nicht abschätzen, wann Clubs und Kneipen wieder öffnen können. Wir hoffen weiterhin alle das Beste und warten darauf, euch endlich wieder als unsere Gäste begrüßen zu dürfen.
