---
layout: post
title:  "Oktober ist großer Geburtstagsmonat!"
date:   2023-10-13
author: Sophie
---

Insgesamt vier Clubs feiern alleine in diesem Monat Ihren Geburtstag.

Angefangen hat der [Kellerklub GAG 18 e.V.](https://www.gag-18.com/) diesen Monat am 6. Oktober mit der Feier um Ihren 49. Geburtstag.

Heute begießen sogar gleich zwei Clubs Ihren Geburtstag! Rund wird es im [Studentenclub Wu 5](https://wu5.de/node/874), die ihren 50. Geburtstag feiern.
Ebenso feiert der [Gutzkowclub](https://gutzkowclub.de/) heute sein 59-jähriges Bestehen und damit auch 59 Jahre Studentenclubs in Dresden!
Es lohnt sich auf jeden Fall, beiden Clubs heute einen Besuch abzustatten und sich anzuschauen, welch schönes Programm beide Clubs auf die Beine gestellt haben.

Zum krönenden Abschluss lädt der [Studentenclub Aquarium am 27.10.](https://club-aquarium.de/2023/10/40-jahre-das-aqua-wir-sagen-dankesch%C3%B6n/) zu seinem 40. Geburtstag ein. Auch hier wird euch so einiges geboten! Neben 40&nbsp;l Pure Sünde für lau, dem weltberühmten Hauscocktail des Aquariums, dürft ihr euch auch auf das musikalische Programm freuen. Motto des Geburtstages ist „Cocktails durch die Jahrzehnte“. Lasst euch überraschen!

Und wie aufmerksame fleißige Leser sicherlich bemerkt haben, feiern zwei der vier Clubs auch nächstes Jahr ihren runden Geburtstag. Wir sind gespannt, was uns hier erwarten darf! Merkt es euch auf jeden Fall vor!
