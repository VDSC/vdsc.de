---
layout: post
title:  "ESE-Clubtour"
date:   2014-10-01 12:00
author: Marc
---

Das neue Wintersemester mit unserem großen [Festmonat][14] steht an. Die Dresdner Studentenclubs freuen sich auf Euch. Erstmal steht aber noch die [ESE-Clubtour][7] besonders für die Erstis auf dem Programm.

[14]: /50
[7]: /eseclubtour
