---
layout: post
title:  "UNI AIR 2023 – der studentische Band- und DJ-Contest"
date:   2023-03-01 14:00
author: Burts
---

Endlich (mal wieder) auf einer großen Bühne stehen und die Massen begeistern? Hier ist eure Chance – das UNI AIR am 24. Mai 2023.

Im Rahmen der [Dresdner Studententage 2023][1] veranstalten das [Studentenwerk Dresden][2] und die [Dresdner Studentenclubs][3] den studentischen Musikwettbewerb [UNI AIR][4] für SolistInnen/Bands und DJs. Sechs SolistInnen/Bands und sechs DJs erhalten am 24. Mai die Chance, sich einem großen Publikum auf der Wiese hinter dem Hörsaalzentrum der TU Dresden zu präsentieren.

Neben der Möglichkeit, endlich wieder auf der Bühne zu stehen, das Licht der Scheinwerfer im Gesicht zu spüren und den Applaus des Publikums zu genießen, haben die Teilnehmer die Chance auf einen der attraktiven Förderpreise des Studentenwerks Dresden von bis zu 1.300 Euro. Noch bis zum 26. März ist die [Bewerbung für einen Startplatz beim diesjährigen UNI AIR][4] möglich.

[1]: https://dresdner-studententage.de
[2]: https://studentenwerk-dresden.de/kultur
[3]: https://vdsc.de/clubs
[4]: https://uni-air.de
