---
layout: post
title:  "Neuer Geschäftsführer im Studentenwerk"
date:   2024-03-11
author: Burts
---
Am 1. März hat [Michael Rollberg][1] das Amt des Geschäftsführers im [Studentenwerk Dresden][2] übernommen. Wir wünschen ihm ein gutes Gelingen und hoffen auf eine gute Zusammenarbeit in den nächsten Jahren. Unser gemeinsames Ziel sollte es sein, die Arbeit der Studentenclubs zu unterstützen und die studentische Kultur in Dresden weiter auszubauen. Wir freuen uns möglichst bald auf ein erstes Kennenlernen.

[1]: https://www.studentenwerk-dresden.de/wirueberuns/newsartikel-6032.html
[2]: https://www.studentenwerk-dresden.de/wirueberuns/
