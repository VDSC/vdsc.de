---
layout: post
title:  "Tharandter Wohnheim leer – noch keine Lösung für den HCC"
date:   2019-04-23 17:00
author: Burts
---

Die große Abschiedsfeier auf dem Studentenwohnheim Weißiger Höhe in Tharandt ist nun vorbei, und bis Ende April ziehen alle Studenten aus. Dem Heinrich-Cotta-Club wurde zwar noch eine Gnadenfrist gewährt, jedoch konnte trotz intensiver Bemühungen des Clubs und des VDSC noch immer keine dauerhafte Lösung für eine Zukunft des Clubs in Tharandt gefunden werden.