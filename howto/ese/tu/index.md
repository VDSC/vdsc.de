---
layout: page
title: Hinweise ESE-Clubtour
menu: ignore
---

Liebe FSRler:innen,

jedes Jahr im Oktober findet die ESE-Clubtour im Rahmen der
Erstsemestereinführungswoche statt. Die ESE-Clubtour wird von den Dresdner
Studentenclubs veranstaltet. Die Koordinierung und Planung mit den Clubs und
den FSR findet über den VDSC (Vereinigung Dresdner Studentenclubs e. V.) statt.

Damit auch nichts in der Planung schief geht, erhaltet ihr hiermit einen
kurzen Abriss, was für die ESE-Clubtour wichtig ist. Bei Fragen dürft ihr euch
natürlich gerne an <em class="honeypot">{{ "now" | date: "%Y-%m-%d" }}@vdsc.de</em>
wenden!

**Termin:** In der Regel der erste Dienstag im Oktober; 19:00 bis 24:00 Uhr \
**Link zur Veranstaltung vom 07.10.2025:** <https://www.vdsc.de/ese>

Im Rahmen der ESE-Clubtour wandern die Fachschaften in Gruppen von
**10–20 Personen** durch die Studentenclubs. Die Gruppen werden jeweils von
**ein bis zwei Tutor:innen** angeführt. Die Tutor:innen
**planen und koordinieren die Route** mit den anderen Tutor:innen aus Ihrem FSR.
Dies soll dafür sorgen, dass nicht in einem Club auf einmal sehr viel Andrang
ist und dafür im nächsten niemand steht.

Um auch einen **Anreiz für die Neustudierenden** zu schaffen, gibt es eine
**Stempelkarte**. Diese ist **im Startbonbon-Heft** zu finden oder könnt ihr
euch in eurem **Startclub abholen**. In **drei Clubs** wird ein **Getränk** zum
**Normalpreis** getrunken. Das **vierte** Getränk im **vierten Club** ist dann
**kostenfrei**. Welche Getränke es für eine volle Stempelkarte gibt, legt jeder
Club für sich **selber** fest. Im Regelfall ist dies ein Getränk freier Wahl,
jedoch maximal im Wert eines Bieres. Bitte beachtet hierbei die Hinweise in den
jeweiligen Clubs oder fragt direkt an der Bar die Mitglieder.

Bitte beachtet, dass auch die Dienste in den Clubs ehrenamtlich arbeitende
Clubmitglieder sind. Gern könnte ihr auch euren neuen Studierenden zu Beginn
kurz erklären, was die Dresdner Studentenclubs eigentlich sind, wie wir
arbeiten und das die Clubs sich jederzeit über neue Mitglieder freuen.

## Stempelkarten

![Stempelkarte vorne]({{ "../../../assets/ese/ese_2025_coupon_vorne.svg" | relative_url }})

![Stempelkarte hinten]({{ "../../../assets/ese/ese_2025_coupon_hinten.svg" | relative_url }})
