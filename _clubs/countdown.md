---
    alias: Studentenklub IZ e.V.
    name: Count Down
    address: Güntzstraße 22, 01307 Dresden
    city: Dresden
    web: https://countdown-dresden.de
    logo_light: assets/logos/countdown/light.png
    lat: 51.0484441
    lon: 13.7569794
    status: member
---
