---
    name: Traumtänzer e.V.
    address: Budapester Straße 24a, 01219 Dresden
    city: Dresden
    web: https://club-traumtaenzer.de
    logo_light: assets/logos/traumtaenzer/light.png
    logo_dark: assets/logos/traumtaenzer/dark.png
    lat: 51.04227
    lon: 13.72110
    status: member
---
