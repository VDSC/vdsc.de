---
    name: Studentenclub Wu5 e.V.
    address: August-Bebel-Straße 12, 01219 Dresden
    city: Dresden
    web: http://wu5.de
    logo_light: assets/logos/wu5/light.png
    logo_dark: assets/logos/wu5/dark.png
    lat: 51.0322532
    lon: 13.7516466
    status: member
---
