---
    name: Club HängeMathe e.V.
    address: Zeunerstraße 1f, 01069 Dresden
    city: Dresden
    web: https://club-haengemathe.de
    logo_light: assets/logos/haengemathe/light.svg
    logo_dark: assets/logos/haengemathe/dark.svg
    lat: 51.0267454
    lon: 13.7367732
    status: member
---
