---
    name: Studierendenclub Maus
    alias: Die Türmer e.V.
    address: Gottfried-Kiesow-Platz 2, 02826 Görlitz
    city: Görlitz
    web: https://www.maus-goerlitz.de
    logo_light: assets/logos/maus/light.png
    logo_dark: assets/logos/maus/dark.png
    lat: 51.1587614
    lon: 14.9919284
    status: associate
---
