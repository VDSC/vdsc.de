---
    name: Gutzkowclub e.V.
    address: Gutzkowstraße 29–33, 01069 Dresden
    city: Dresden
    web: http://gutzkowclub.de
    logo_light: assets/logos/gutzkowclub/light.png
    logo_dark: assets/logos/gutzkowclub/dark.png
    lat: 51.0328076
    lon: 13.7401300
    status: member
---
