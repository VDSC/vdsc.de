---
layout: page
title: Vereinigung Dresdner Studentenclubs e. V.
menu: Verein
order: 2
---

Die Vereinigung Dresdner Studentenclubs (VDSC) ist der Dachverband der Dresdner Studentenclubs. Der VDSC dient als gemeinsame Interessenvertretung, unterstützt die Arbeit der Clubs und koordiniert gemeinsame Aktivitäten. So treffen sich die Clubs im Regelfall monatlich im Rahmen einer gemeinsamen Planungsrunde, der VDSC-Sitzung, zu der alle Studentenclubs eingeladen sind.

Der VDSC besteht seit 2007 in loser Organisationsform. 2017 wurde ein Verein gegründet. Die einzelnen Clubs bleiben jedoch unabhängige, eigenständig arbeitende Institutionen.

# Gemeinnützigkeit

Der VDSC e.V. hat den gemeinnützigen Zweck, das bürgerschaftliche und soziale Engagement der Dresdner Studentenclubs zu fördern. Näheres dazu in der [Satzung](satzung.pdf).

Deshalb ist der VDSC e.V. berechtigt, für Spenden, die uns zur Verwendung für diesen Zweck zugewendet werden, Zuwendungsbestätigungen nach amtlich vorgeschriebenem Vordruck auszustellen. Dies wurde uns vom Finanzamt Dresden-Süd bescheinigt.

# Mitglieder

<ul>{% for club in site.clubs %}

  {% if club.status == "member" %}
    <li>
      <strong>{{ club.name }}</strong><br/>
      <p>{% if club.alias %}({{ club.alias}})<br/>{% endif %}{{ club.address}}<br/>
      <a href="{{ club.web }}">{{ club.web | remove:'http://' | remove:'https://' }}</a></p>
    </li>
  {% endif %}

{% endfor %}</ul>


# Assoziierte Mitglieder

<ul>{% for club in site.clubs %}

  {% if club.status == "associate" %}
    <li>
      <strong>{{ club.name }}</strong><br/>
      <p>{% if club.alias %}({{ club.alias}})<br/>{% endif %}{{ club.address}}<br/>
      <a href="{{ club.web }}">{{ club.web | remove:'http://' | remove:'https://' }}</a></p>
    </li>
  {% endif %}

{% endfor %}</ul>
